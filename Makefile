destroy:
	@vagrant destroy -f
	@rm -rf tmp_deploying_stage

create:
	@vagrant up -d

recreate:
	@make destroy
	@make create

stop:
	@VBoxManage controlvm jenkins acpipowerbutton

start:
	@VBoxManage startvm jenkins --type headless

status:
	@VBoxManage list runningvms
