# -*- mode: ruby -*-
# vi: set ft=ruby :

# Require YAML module
require 'yaml'

def noprovisioned?(vm_name='default', provider='virtualbox')
  File.exist?(".vagrant/machines/#{vm_name}/#{provider}/action_provision")
end


config = YAML.load_file(File.join(File.dirname(__FILE__), 'config.yml'))

base_box=config['environment']['base_box']

#engine_version=config['environment']['engine_version']

#swarm_master_ip=config['environment']['swarm_masterip']

domain=config['environment']['domain']

proxy=config['environment']['proxy']

boxes = config['boxes']

boxes_hostsfile_entries=""

 boxes.each do |box|
   boxes_hostsfile_entries=boxes_hostsfile_entries+box['mgmt_ip'] + ' ' +  box['name'] + ' ' + box['name']+'.'+domain+'\n'
 end

#puts boxes_hostsfile_entries

update_hosts = <<SCRIPT
    echo "127.0.0.1 localhost" >/etc/hosts
    echo -e "#{boxes_hostsfile_entries}" |tee -a /etc/hosts
SCRIPT

Vagrant.configure(2) do |config|
  config.vm.box = base_box
  config.vm.synced_folder "tmp_deploying_stage/", "/tmp_deploying_stage",create:true
  config.vm.synced_folder "src/", "/src",create:true
  boxes.each do |node|
    config.vm.define node['name'] do |config|
    if Vagrant.has_plugin?("vagrant-proxyconf")
        if proxy
              config.proxy.http     = proxy
              config.proxy.https     = proxy
              config.proxy.no_proxy = "localhost,127.0.0.1"
        end
    end

     config.vm.hostname = node['name']
     config.vm.provider "virtualbox" do |v|
     v.name = node['name']
     v.customize ["modifyvm", :id, "--memory", node['mem']]
     v.customize ["modifyvm", :id, "--cpus", node['cpu']]

     v.customize ["modifyvm", :id, "--nictype1", "Am79C973"]
     v.customize ["modifyvm", :id, "--nictype2", "Am79C973"]
     v.customize ["modifyvm", :id, "--nictype3", "Am79C973"]
     v.customize ["modifyvm", :id, "--nictype4", "Am79C973"]
     v.customize ["modifyvm", :id, "--nicpromisc1", "allow-all"]
     v.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"]
     v.customize ["modifyvm", :id, "--nicpromisc3", "allow-all"]
     v.customize ["modifyvm", :id, "--nicpromisc4", "allow-all"]
     v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]

     end

    config.vm.network "private_network",
    ip: node['mgmt_ip'],
    virtualbox__intnet: "DEV"

    config.vm.network "public_network",
    bridge: ["enp4s0","wlp3s0","enp3s0f1","wlp2s0"],
    auto_config: true


    config.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update -qq && apt-get install -qq chrony && timedatectl set-timezone Europe/Madrid
    SHELL

    config.vm.provision :shell, :inline => update_hosts

    puts '--------------------------------' if noprovisioned?
    puts 'node role '+node['role'] if noprovisioned?


    #JENKINS
    case node['role']
      when "jenkins-master"
          config.vm.network "forwarded_port", guest: 8080, host: 8080, auto_correct: true


          # If we first install docker and create jenkins user, jenkins daemon could have access to docker daemon for building and running containers
          #sudo groupadd jenkins
          #sudo useradd -g jenkins -G docker -d /var/lib/jenkins -s /bin/bash -c "Jenkins" jenkins

          config.vm.provision "shell", inline: <<-SHELL
            sudo apt-get install --no-install-recommends -qq curl
            curl -sSL https://get.docker.com| sudo sh
            sudo systemctl restart docker
            sudo usermod -aG docker ubuntu
            curl -o /usr/local/bin/docker-compose -sSL https://github.com/docker/compose/releases/download/1.10.0-rc1/docker-compose-`uname -s`-`uname -m`
            chmod +x /usr/local/bin/docker-compose
          SHELL


          config.vm.provision "shell", inline: <<-SHELL
            wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
            sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
            sudo apt-get update -qq && sudo apt-get install --no-install-recommends -qq jenkins && sudo systemctl enable jenkins
            cp /var/lib/jenkins/secrets/initialAdminPassword /tmp_deploying_stage/jenkins_initialAdminPassword
            sudo usermod -aG docker jenkins
            sudo systemctl restart jenkins
          SHELL
          if File.file?('/tmp_deploying_stage/jenkins_initialAdminPassword')
              puts '-------Initial Jenkins Admin Password -------------------------'
              puts File.read('/tmp_deploying_stage/jenkins_initialAdminPassword')
              puts '---------------------------------------------------------------'
          end

        when "gitlab-master"
            config.vm.network "forwarded_port", guest: 80, host: 9080, auto_correct: true
            config.vm.network "forwarded_port", guest: 443, host: 9443, auto_correct: true

          config.vm.provision "shell", inline: <<-SHELL
            sudo apt-get update -qq && sudo apt-get install --no-install-recommends -qq ca-certificates apt-transport-https
            sudo sh -c 'echo "deb https://packages.gitlab.com/gitlab/gitlab-ce/ubuntu/ `lsb_release -cs` main" > /etc/apt/sources.list.d/gitlab_gitlab-ce.list'
            wget -q -O - https://packages.gitlab.com/gpg.key | sudo apt-key add -
            sudo apt-get update && sudo apt-get install -qq --no-install-recommends gitlab-ce
            sudo gitlab-ctl reconfigure
          SHELL

      else
        puts 'Unknown role '+node['role']
        puts 'Unknown role '+node['role'] if noprovisioned?("TEST")
    end
    puts '--------------------------------' if noprovisioned?

      #config.vm.provision "file", source: "create_swarm.sh", destination: "/tmp/create_swarm.sh"
      #config.vm.provision :shell, :path => 'create_swarm.sh' , :args => [ node['mgmt_ip'], node['swarm_role'], swarm_master_ip, engine_version ]

      #config.vm.provision "file", source: "install_compose.sh", destination: "/tmp/install_compose.sh"
      #config.vm.provision :shell, :path => 'install_compose.sh'
    end
  end

end
